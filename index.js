"use strict";
const sense = require("sense-hat-led").sync;
const app = require('express')()
const http = require('http')
http.Server(app)
//const io = require('socket.io')(http)
const ip = require('ip')

var myArgs = process.argv.slice(2);

/*
const ID= Number(myArgs[0])
if (!Number.isInteger(ID)) {
  console.log("Le numero du script est obligatoire")
  return 0
}
const port = Number(myArgs[1]) || 3000+ID
*/
var ID=0
const port=3000

const myip = ip.address()
const mynetwork = ip.subnet(myip, '255.255.255.0');// masque en /24 par defaut
console.log("IP :",myip)
const [o1,o2,o3,o4] = myip.toString().split(".")
let fromO = mynetwork.firstAddress.split(".").pop()
let toO = mynetwork.lastAddress.split(".").pop()
console.log(o1,o2,o3,fromO,toO)
let ballColors=[[255,0,0],[0,255,0],[0,0,255],[255,255,0],[0,255,255],[255,0,255]]
let currentColorId=0
//let ballcolor = ballColors[0]


let team = [
{id:1,ip:"172.31.43.125",port:3000,ready:false},
{id:2,ip:"172.31.43.126",port:3000,ready:false},
{id:3,ip:"172.31.43.153",port:3000,ready:false},
{id:4,ip:"172.31.43.154",port:3000,ready:false},
{id:5,ip:"172.31.43.155",port:3000,ready:false}]



team.forEach(e=>{
  let ipid=e.ip.split(".")[3]
  console.log(ipid,o4)
  if (e.ip.split(".")[3]==o4){
    ID=e.id;
    e.ready=true
  }

})

team.isReady = function() {
  //console.log("team",this)
  return this.reduce((i,e)=>i && e.ready,true)
};

function checkTeams(t,callback){
  setTimeout(()=>{
    team.forEach(e=>{
      console.log("check ",e.id,e.ready)
      if(!e.ready){
        let url = 'http://'+e.ip+':'+e.port+"/"
        console.log(url)
        http.get(url,(res)=>{
          res.setEncoding('utf8');
          let data = ''
          res.on("data", d => {
            data += d
            console.log("adding",d)
          })
          res.on("end", () => {            
            console.log("received response from ",e.ip," :",e.port, data )
            try {
              const jsonResponse = JSON.parse(data);
              let copainID = jsonResponse.id
	      console.log(copainID)
              if(Number.isInteger(copainID)){
                console.log("On valide le copain")
                team[copainID-1].ready=true
              }else{
                throw new Error("Pas un copain...")
              }
            } catch (e) {
              console.error(e)
            }
          })
        }).on("error", function (err){console.log("GET request error",err)});
      }
    })

    if(team.isReady()){
      callback()
    }else{
      checkTeams(1000,callback)
    }
  },t)
}

function playGame(x,y,dx,dy,colorId){
  
  currentColorId=colorId

  if (x==0){
    dx=1
  }
  if (x==7){
    dx=-1  
  }

  //TODO gérer le X plus tard
  if (Math.floor(y/8)+1 == ID){
    
    console.log("pixel",x,y,dx,dy)
    let py = y%8
    //on est chez nous
    sense.clear()
    sense.setPixel(x, py,ballColors[currentColorId])
    // rebond 
    if(ID==1 && y==0){
      console.log("reverse")
      currentColorId=(currentColorId+1)%6
      dy=1;
    }
    //
    if(ID==5 && py==7){
      console.log("reverse")
      currentColorId=(currentColorId+1)%6
      dy=-1
    }
    y+=dy
    x+=dx

    //changement de pi
    if(Math.floor(y/8)+1 < ID){

      let url = 'http://'+team[ID-2].ip+':'+team[ID-2].port+"/"+x+"/"+y+"/"+dx+"/"+dy+"/"+currentColorId
      console.log(url)
      setTimeout(()=>{
        http.get(url,(res)=>{
          sense.clear()
          console.log("response from ",(ID-1),res.statusCode)
        })
      },100)

    }else if (Math.floor(y/8)+1 > ID){
 
      let url = 'http://'+team[ID].ip+':'+team[ID].port+"/"+x+"/"+y+"/"+dx+"/"+dy+"/"+currentColorId
        console.log(url)
        setTimeout(()=>{
          http.get(url,(res)=>{
	    sense.clear()	
            console.log("response from ",(ID+1),res.statusCode)
          })
	},100)

    }else{
      setTimeout(()=>{
        playGame(x,y,dx,dy,currentColorId)
      },100);
    }
  }
}



app.get('/', (req, res) => {
  res.json({id:ID});
})

app.get('/:x/:y/:dx/:dy/:color', (req, res) => {
  console.log("receive",req.param)  
  playGame(req.params.x*1,req.params.y*1,req.params.dx*1,req.params.dy*1,req.params.color*1)
  res.json({message:"OK"});
})

app.listen(port, () => {
  console.log("server",ID,"listen on",port)
  sense.clear();
  checkTeams(1,function(){
    // Check for deconnexion
    console.log("Les copains sont prêt, on va pouvoir commencer")

    playGame(3,3,1,1,currentColorId)

  })
  

})
